<footer class="footer footer-dark blog-footer">
    <div class="f-widget-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="f-widget">
                        <div class="f-logo">
                            <img src="assets/images/logo-bd.png" alt="">
                        </div>
                        <p>Silo triple bottom line, optimism relief invest co-creation accessibility impact
                            investing white paper program area!</p>
                        <div class="f-newsletter">
                            <div class="input-group">
                                <input type="email" class="form-control" placeholder="Email address">
                                <span class="input-group-btn">
                                    <button type="button"><i class="fa fa-paper-plane-o"></i></button>
                                </span>
                            </div>
                        </div>
                        <ul class="f-contact-list">
                            <li>
                                <span>Follow Us:</span>
                                <ul class="social-profile">
                                    <li><a href="#"><i class="social_facebook"></i></a></li>
                                    <li><a href="#"><i class="social_twitter"></i></a></li>
                                    <li><a href="#"><i class="social_vimeo"></i></a></li>
                                    <li><a href="#"><i class="social_linkedin"></i></a></li>
                                    <li><a href="#"><i class="social_dribbble"></i></a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1 offset-md-1 col-md-5 col-sm-6">
                    <div class="f-widget">
                        <h3 class="f-widget-title">Support</h3>
                        <ul class="f-list">
                            <li><a href="#">Documentation</a></li>
                            <li><a href="#">FAQ's</a></li>
                            <li><a href="#">Privacy</a></li>
                            <li><a href="#">Support Forum</a></li>
                            <li><a href="#">Themes</a></li>
                            <li><a href="#">Account</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="f-widget">
                        <h3 class="f-widget-title">Company</h3>
                        <ul class="f-list">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">News</a></li>
                            <li><a href="#">Customers</a></li>
                            <li><a href="#">Plugins</a></li>
                            <li><a href="#">Quick Start Guide</a></li>
                            <li><a href="#">Feedback</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-0 offset-md-1 col-md-5 col-sm-6">
                    <div class="f-widget">
                        <h3 class="f-widget-title">Instagram</h3>
                        <ul class="f-instagram">
                            <li><a href="#"><img src="assets/images/insta1.png" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta2.png" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta3.png" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta4.png" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta5.png" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta6.png" alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="f-copyright-area">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-6">
                    <p class="copyright text-center text-md-left mb-2 mb-md-0">© 2018 <a href="#">Appbiz</a> ALL
                        RIGHTS RESERVED</p>
                </div>
                <div class="col-md-6">
                    <ul class="nav f-nav justify-content-md-end justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link" href="#">ABOUT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">HELP</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">CONTACT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">TERMS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">PRIVACY</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer><!-- footer -->
