<header class="header">
    <nav class="navbar navbar-expand-lg fixed-top" id="main-nav">
        <div class="container">
            <a class="navbar-brand" href="index.html">
                <img class="white-logo" src="assets/images/logo-g.png" alt="">
                <img class="color-logo" src="assets/images/logo.png" alt="">
            </a>
            <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#main-nav-collapse"
                aria-controls="main-nav-collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="menu-toggle">
                    <span class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                    <span class="hamburger-cross">
                        <span></span>
                        <span></span>
                    </span>
                </span>
            </button>
            <div class="collapse navbar-collapse order-3 order-lg-2" id="main-nav-collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link nav-link-scroll" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-scroll" href="#feature">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-scroll" href="#screenshot">Screenshots</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link nav-link-scroll" href="#pricing">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-scroll" href="#download">Download</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-link-scroll" href="#blog">News</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header><!-- header -->
