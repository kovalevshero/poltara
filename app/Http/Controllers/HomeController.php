<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $ret['datas'] = News::all();

        return view('landingpage/home', $ret);
    }

    public function newsDetail(Request $request)
    {
        $data = $request->all();

        $ret['datas'] = News::where('title', $data['title'])->first();

        return view('landingpage/detail', $ret);
    }
}
