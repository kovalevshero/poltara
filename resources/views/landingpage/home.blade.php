@extends('landingpage-layout.default')
@section('content')
    <div class="page-title">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <div class="page-title-content align-items-center">
                        <h1 class="page-name">Become smarter in just 5 minutes</h1>
                        <ol class="breadcrumb">
                            <!-- <li><span>Get the daily email that makes reading the news actually
                                                                                                                                                                                                                                                                                         enjoyable. Stay informed and entertained, for free.</span></li> -->
                            <li class="item-current"><span>Get the daily email that makes reading the news actually
                                    enjoyable. Stay informed and entertained, for free.</span></li>
                        </ol>
                    </div>
                    <div class="input-group my-4 mx-4">
                        <input type="text" class="form-control" placeholder="Enter Email"
                            aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn" type="button"
                                style="background-color: rgb(25, 21, 77) !important;">Subscribe</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5">
                </div>
            </div>
        </div>
    </div>
    <div class="blog-page-main-wrapper">
        <div class="container">
            <div class="row right-sidebar">
                <h2 class="post-title">Featured</h2>
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="img-center h2-about-img-wrapper">
                            <img src="https://www.politico.com/dims4/default/848b42c/2147483647/strip/true/crop/3000x1999+0+0/resize/630x420!/quality/90/?url=https%3A%2F%2Fstatic.politico.com%2F10%2Fc7%2Fdf804f074a81ab251a15c656180f%2F211105-lina-khan-ap-773.jpg"
                                class="about-img1 wow slideInUpAlt" alt="">
                        </div>
                    </div>
                    <div class="col-lg-5 offset-lg-2">
                        <div class="about-content">
                            <h2>How a long-fought Democratic win could pose trouble for Amazon and Elon Musk </h2>
                            <p>With the confirmation of a third Democrat to the Federal Trade Commission, the
                                progressive chair regains the agency’s majority — and the ability to speed ahead
                                with her priorities.</p>
                            <div class="btn-wrapper">
                                <a href="#" class="btn fill-style">READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
                <main class="col-12 main-content">
                    <div class="post-row">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <div class="blog-post style-2">
                                    <div class="post-thumb">
                                        <a href="#"><img src="assets/images/blog12.png" alt=""></a>
                                        <span class="meta-date"><span class="date">16</span> Apr</span>
                                    </div>
                                    <div class="post-info">
                                        <h2 class="post-title"><a href="#">Adipicing perferendis lectus debitis.</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="meta-author"><i class="fa fa-user-o"></i> Admin</a></span>
                                            <span class="meta-date"><i class="fa fa-calendar-times-o"
                                                    aria-hidden="true"></i> 16 October, 2018</span>
                                            <span class="meta-category"><i class="fa fa-folder-o" aria-hidden="true"></i>
                                                <a href="#">Business</a></span>
                                        </div>
                                        <div class="post-des">
                                            Nec lacinia. Quod dicta donec perspiciatis rutrum saepe egestas, porro
                                            repellat
                                            curabitur, blandit rerum nostrud id conubia phasellus officiis.!
                                        </div>
                                        <a class="read-more-btn" href="#">Read More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="blog-post style-2">
                                    <div class="post-thumb">
                                        <a href="#"><img src="assets/images/blog12.png" alt=""></a>
                                        <span class="meta-date"><span class="date">16</span> Apr</span>
                                    </div>
                                    <div class="post-info">
                                        <h2 class="post-title"><a href="#">Adipicing perferendis lectus debitis.</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="meta-author"><i class="fa fa-user-o"></i> Admin</a></span>
                                            <span class="meta-date"><i class="fa fa-calendar-times-o"
                                                    aria-hidden="true"></i> 16 October, 2018</span>
                                            <span class="meta-category"><i class="fa fa-folder-o" aria-hidden="true"></i>
                                                <a href="#">Business</a></span>
                                        </div>
                                        <div class="post-des">
                                            Nec lacinia. Quod dicta donec perspiciatis rutrum saepe egestas, porro
                                            repellat
                                            curabitur, blandit rerum nostrud id conubia phasellus officiis.!
                                        </div>
                                        <a class="read-more-btn" href="#">Read More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <div class="blog-post style-2">
                                    <div class="post-thumb">
                                        <a href="#"><img src="assets/images/blog12.png" alt=""></a>
                                        <span class="meta-date"><span class="date">16</span> Apr</span>
                                    </div>
                                    <div class="post-info">
                                        <h2 class="post-title"><a href="#">Adipicing perferendis lectus debitis.</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="meta-author"><i class="fa fa-user-o"></i> Admin</a></span>
                                            <span class="meta-date"><i class="fa fa-calendar-times-o"
                                                    aria-hidden="true"></i> 16 October, 2018</span>
                                            <span class="meta-category"><i class="fa fa-folder-o" aria-hidden="true"></i>
                                                <a href="#">Business</a></span>
                                        </div>
                                        <div class="post-des">
                                            Nec lacinia. Quod dicta donec perspiciatis rutrum saepe egestas, porro
                                            repellat
                                            curabitur, blandit rerum nostrud id conubia phasellus officiis.!
                                        </div>
                                        <a class="read-more-btn" href="#">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <main class="col-lg-9 main-content">
                    <div class="post-row">
                        <h2 class="blog-single-post blog-post post-title">Latest Stories</h2>
                        <div class="row gutter-40">
                            {{-- @foreach ($datas as $data)
                                <div class="col-md-6">
                                    <div class="blog-post style-2">
                                        <div class="post-thumb">
                                            <a href="#"><img src="{{ $data->image }}" alt=""></a>
                                            <span class="meta-date"><span class="date">16</span> Apr</span>
                                        </div>
                                        <div class="post-info">
                                            <h2 class="post-title"><a href="#">{{ $data->title }}</a>
                                            </h2>
                                            <div class="post-meta">
                                                <span class="meta-author"><i class="fa fa-user-o"></i>
                                                    {{ $data->created_by }}</a></span>
                                                <span class="meta-category"><i class="fa fa-folder-o"
                                                        aria-hidden="true"></i>
                                                    <a href="#">{{ $data->category }}</a></span>
                                                <span class="meta-date"><i class="fa fa-calendar-times-o"
                                                        aria-hidden="true"></i>
                                                    {{ date_format($data->created_at, 'd-m-Y') }}</span>
                                            </div>
                                            <div class="post-des">
                                                {{ $data->short_description }}
                                            </div>
                                            <a class="read-more-btn" href="{{ url('news?q=') . $data->title }}">Read
                                                More</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach --}}
                            <div class="col-md-6">
                                <div class="blog-post style-2">
                                    <div class="post-thumb">
                                        <a href="#"><img
                                                src="https://www.politico.com/dims4/default/848b42c/2147483647/strip/true/crop/3000x1999+0+0/resize/630x420!/quality/90/?url=https%3A%2F%2Fstatic.politico.com%2F10%2Fc7%2Fdf804f074a81ab251a15c656180f%2F211105-lina-khan-ap-773.jpg"
                                                alt=""></a>
                                        <span class="meta-date"><span class="date">16</span>
                                            Apr</span>
                                    </div>
                                    <div class="post-info">
                                        <h2 class="post-title"><a href="#">How a long-fought Democratic win could pose
                                                trouble for Amazon and Elon Musk</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="meta-author"><i class="fa fa-user-o"></i>
                                                Admin</a></span>
                                            <span class="meta-category"><i class="fa fa-folder-o" aria-hidden="true"></i>
                                                <a href="#">Business</a></span>
                                            <span class="meta-date"><i class="fa fa-calendar-times-o"
                                                    aria-hidden="true"></i>
                                                16 October, 2018</span>
                                        </div>
                                        <div class="post-des">
                                            With the confirmation of a third Democrat to the Federal Trade Commission,
                                            the
                                            progressive chair regains the agency’s majority — and the ability to speed
                                            ahead
                                            with her priorities
                                        </div>
                                        <a class="read-more-btn" href="#">Read
                                            More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="blog-post style-2">
                                    <div class="post-thumb">
                                        <a href="#"><img
                                                src="https://www.politico.com/dims4/default/848b42c/2147483647/strip/true/crop/3000x1999+0+0/resize/630x420!/quality/90/?url=https%3A%2F%2Fstatic.politico.com%2F10%2Fc7%2Fdf804f074a81ab251a15c656180f%2F211105-lina-khan-ap-773.jpg"
                                                alt=""></a>
                                        <span class="meta-date"><span class="date">16</span>
                                            Apr</span>
                                    </div>
                                    <div class="post-info">
                                        <h2 class="post-title"><a href="#">How a long-fought Democratic win could pose
                                                trouble for Amazon and Elon Musk</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="meta-author"><i class="fa fa-user-o"></i>
                                                Admin</a></span>
                                            <span class="meta-category"><i class="fa fa-folder-o" aria-hidden="true"></i>
                                                <a href="#">Business</a></span>
                                            <span class="meta-date"><i class="fa fa-calendar-times-o"
                                                    aria-hidden="true"></i>
                                                16 October, 2018</span>
                                        </div>
                                        <div class="post-des">
                                            With the confirmation of a third Democrat to the Federal Trade Commission,
                                            the
                                            progressive chair regains the agency’s majority — and the ability to speed
                                            ahead
                                            with her priorities
                                        </div>
                                        <a class="read-more-btn" href="#">Read
                                            More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="blog-post style-2">
                                    <div class="post-thumb">
                                        <a href="#"><img
                                                src="https://www.politico.com/dims4/default/848b42c/2147483647/strip/true/crop/3000x1999+0+0/resize/630x420!/quality/90/?url=https%3A%2F%2Fstatic.politico.com%2F10%2Fc7%2Fdf804f074a81ab251a15c656180f%2F211105-lina-khan-ap-773.jpg"
                                                alt=""></a>
                                        <span class="meta-date"><span class="date">16</span>
                                            Apr</span>
                                    </div>
                                    <div class="post-info">
                                        <h2 class="post-title"><a href="#">How a long-fought Democratic win could pose
                                                trouble for Amazon and Elon Musk</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="meta-author"><i class="fa fa-user-o"></i>
                                                Admin</a></span>
                                            <span class="meta-category"><i class="fa fa-folder-o" aria-hidden="true"></i>
                                                <a href="#">Business</a></span>
                                            <span class="meta-date"><i class="fa fa-calendar-times-o"
                                                    aria-hidden="true"></i>
                                                16 October, 2018</span>
                                        </div>
                                        <div class="post-des">
                                            With the confirmation of a third Democrat to the Federal Trade Commission,
                                            the
                                            progressive chair regains the agency’s majority — and the ability to speed
                                            ahead
                                            with her priorities
                                        </div>
                                        <a class="read-more-btn" href="#">Read
                                            More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="blog-post style-2">
                                    <div class="post-thumb">
                                        <a href="#"><img
                                                src="https://www.politico.com/dims4/default/848b42c/2147483647/strip/true/crop/3000x1999+0+0/resize/630x420!/quality/90/?url=https%3A%2F%2Fstatic.politico.com%2F10%2Fc7%2Fdf804f074a81ab251a15c656180f%2F211105-lina-khan-ap-773.jpg"
                                                alt=""></a>
                                        <span class="meta-date"><span class="date">16</span>
                                            Apr</span>
                                    </div>
                                    <div class="post-info">
                                        <h2 class="post-title"><a href="#">How a long-fought Democratic win could pose
                                                trouble for Amazon and Elon Musk</a>
                                        </h2>
                                        <div class="post-meta">
                                            <span class="meta-author"><i class="fa fa-user-o"></i>
                                                Admin</a></span>
                                            <span class="meta-category"><i class="fa fa-folder-o" aria-hidden="true"></i>
                                                <a href="#">Business</a></span>
                                            <span class="meta-date"><i class="fa fa-calendar-times-o"
                                                    aria-hidden="true"></i>
                                                16 October, 2018</span>
                                        </div>
                                        <div class="post-des">
                                            With the confirmation of a third Democrat to the Federal Trade Commission,
                                            the
                                            progressive chair regains the agency’s majority — and the ability to speed
                                            ahead
                                            with her priorities
                                        </div>
                                        <a class="read-more-btn" href="#">Read
                                            More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pagination">
                        <a class="prev page-numbers" href="#"><i class="fa fa-angle-left"></i></a>
                        <a class="page-numbers" href="#"><span>1</span></a>
                        <span class="page-numbers current"><span>2</span></span>
                        <a class="page-numbers" href="#"><span>3</span></a>
                        <a class="next page-numbers" href="#"><i class="fa fa-angle-right"></i></a>
                    </div>
                </main>
                <aside class="col-lg-3 sidebar">
                    <div class="widget widget-search">
                        <form role="search" class="widget-search-form">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search.." aria-label="Search for..."
                                    autocomplete="off">
                                <span class="input-group-btn">
                                    <button class="btn" type="button"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="widget popular-post-widget">
                        <h2 class="widget-title">Popular Post</h2>
                        <ul>
                            <li>
                                <article class="widget-popular-post">
                                    <a href="#">
                                        <div class="t-cell">
                                            <div class="post-img">
                                                <img src="assets/images/b-thumb1.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="t-cell">
                                            <div class="post-detail">
                                                <p>Ridiculus aptet, quisque nisl delectus fugit </p>
                                                <span>March 03, 2018</span>
                                            </div>
                                        </div>
                                    </a>
                                </article>
                            </li>
                            <li>
                                <article class="widget-popular-post">
                                    <a href="#">
                                        <div class="t-cell">
                                            <div class="post-img">
                                                <img src="assets/images/b-thumb2.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="t-cell">
                                            <div class="post-detail">
                                                <p>Ridiculus aptet, quisque nisl delectus fugit </p>
                                                <span>March 03, 2018</span>
                                            </div>
                                        </div>
                                    </a>
                                </article>
                            </li>
                            <li>
                                <article class="widget-popular-post">
                                    <a href="#">
                                        <div class="t-cell">
                                            <div class="post-img">
                                                <img src="assets/images/b-thumb3.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="t-cell">
                                            <div class="post-detail">
                                                <p>Ridiculus aptet, quisque nisl delectus fugit </p>
                                                <span>March 03, 2018</span>
                                            </div>
                                        </div>
                                    </a>
                                </article>
                            </li>
                            <li>
                                <article class="widget-popular-post">
                                    <a href="#">
                                        <div class="t-cell">
                                            <div class="post-img">
                                                <img src="assets/images/b-thumb4.jpg" alt="">
                                            </div>
                                        </div>
                                        <div class="t-cell">
                                            <div class="post-detail">
                                                <p>Ridiculus aptet, quisque nisl delectus fugit </p>
                                                <span>March 03, 2018</span>
                                            </div>
                                        </div>
                                    </a>
                                </article>
                            </li>
                        </ul>
                    </div>
                    <div class="widget widget-archive">
                        <h2 class="widget-title">Archives</h2>
                        <ul class="widget-archive-list">
                            <li class="cat-item"><a href="#"><span>Fedruary 2018 </span> <span>(25)</span></a>
                            </li>
                            <li class="cat-item"><a href="#"><span>March 2018 </span> <span>(27)</span></a> </li>
                            <li class="cat-item"><a href="#"><span>November 2018 </span> <span>(30)</span></a>
                            </li>
                            <li class="cat-item"><a href="#"><span>April 2018 </span> <span>(35)</span></a> </li>
                            <li class="cat-item"><a href="#"><span>January 2018 </span> <span>(48)</span></a>
                            </li>
                            <li class="cat-item"><a href="#"><span>July 2018 </span> <span>(18)</span></a> </li>
                        </ul>
                    </div>
                    <div class="widget widget-flickr">
                        <h2 class="widget-title">Flickr Widget</h2>
                        <ul class="f-instagram">
                            <li><a href="#"><img src="assets/images/insta7.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta8.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta9.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta10.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta11.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta12.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta13.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta14.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/insta15.jpg" alt=""></a></li>
                        </ul>
                    </div>
                    <div class="widget widget-tags">
                        <h2 class="widget-title">Tags Widget</h2>
                        <div class="tagcloud">
                            <a href="#">Portfolio</a>
                            <a href="#">Mobile Ui</a>
                            <a href="#">Flat</a>
                            <a href="#">Awesome</a>
                            <a href="#">iOS</a>
                            <a href="#">Windows</a>
                            <a href="#">MTML</a>
                            <a href="#">Ui Design</a>
                            <a href="#">Branding</a>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
@endsection
