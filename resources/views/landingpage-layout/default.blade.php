<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Poltara</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/lightcase.css">
    <link rel="stylesheet" href="assets/css/simple-line-icons.css">
    <link rel="stylesheet" href="assets/css/ElegantIcons.css">
    <link rel="stylesheet" href="assets/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
</head>

<body data-spy="scroll" data-target=".navbar">
    @include('landingpage-layout.header')
    @yield('content')
    @include('landingpage-layout.footer')

    <div class="scroll-top"><i class="arrow_carrot-up"></i></div>

    <div class="main-search-area">
        <form class="main-search-form full-view">
            <div class="m-s-input">
                <input type="search" name="search" class="search-input" placeholder="Search..." autocomplete="off">
            </div>
            <span>Type and Hit Enter to Search</span>
        </form>
        <i class="icon_close"></i>
    </div>

    <script src="assets/js/jquery-2.2.3.min.js"></script>
    <script src="assets/js/jquery.easing.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/swiper.min.js"></script>
    <script src="assets/js/jquery.events.touch.min.js"></script>
    <script src="assets/js/lightcase.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/TweenMax.min.js"></script>
    <script src="assets/js/jquery.wavify.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.parallax-scroll.js"></script>
    <script src="assets/js/particles.min.js"></script>
    <script src="assets/js/jarallax.min.js"></script>
    <script src="assets/js/jarallax-video.min.js"></script>
    <script src="assets/js/form-validator.js"></script>
    <script src="assets/js/custom.js"></script>
</body>

</html>
